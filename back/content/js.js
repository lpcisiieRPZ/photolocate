$(document).ready(function () {
    $('select').material_select();

    var map = L.map('map');
    var data = new Object();
    data.serie = {};
    data.serie.lat = 48.856614;
    data.serie.lon = 2.3522219;
    data.serie.zoom = 4;
    data.serie.label = 'Paris';


    var uri = window.location.href;
    var rootUri = uri.split('/');
    var root = "";
    var i=0;

    while(rootUri[i] != "back"){
        root += rootUri[i] + '/';
        i++;
    }

    $('select').change(function () {

        $("#selectValue option:selected").each(function () {
            var id = $(this).val();
            $.get(root +'api/series/' + id, {

            }).then(function (response) {
                    data.serie.lat = response.serie.lat;
                    data.serie.lon = response.serie.lon;
                    data.serie.zoom = response.serie.zoom;
                    data.serie.label = response.serie.label;

                    $('#latitude').val(data.serie.lat);
                    $('#longitude').val(data.serie.lon);
                    $('#zoom').val(data.serie.zoom);


                    init(data);
                });
        });
    });

    var init = function(data){
        map.remove();
        var marker;
        var ville = data.serie.label;
        var latitude = data.serie.lat;
        var longitude = data.serie.lon;
        var zoom = data.serie.zoom;


        // on affiche la carte
        map = L.map('map').setView([latitude, longitude], zoom);
        marker = L.marker([latitude, longitude]).addTo(map);
        marker.bindPopup(ville).openPopup();
        //console.log('ville : '+ville);
        //console.log('latitude : '+latitude);
        //console.log('longitude : ' +longitude);
        //console.log('zoom : '+zoom);

        //  Ajout d'un layer de carte
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        // modification de la position du marker
        map.on('click', function(ev) {
            map.removeLayer(marker);
            marker = new L.marker(ev.latlng).addTo(map);
            latitude = ev.latlng.lat;
            longitude = ev.latlng.lng;
            zoom = map.getZoom();
            $('#zoom').val(zoom);
            $('#latitude').val(latitude);
            $('#longitude').val(longitude);
        });
    };

    init(data);
});

var photo = document.getElementById("photo");
photo.onclick = function(){
    lat = document.getElementById("latitude").value;
    lon = document.getElementById("longitude").value;
    if(lat == "" && lon == ""){
        swal("Oops...", "Veuillez saisir les informations et la photo...!", "error")
    }else {
        swal("La photo a bien ete ajoutee!", "", "success")
    }
};
