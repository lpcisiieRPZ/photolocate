var latitude;
var longitude;
var map;
var ville = "Paris";
var marker;
var zoom = 4;

var coord = function(){
    // on récupère la position de la ville souhaitée
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "https://maps.googleapis.com/maps/api/geocode/json?address="+ville, false);
    xhr.send();
    var response = JSON.parse(xhr.response);

    if(response.status == "OK"){
        latitude = response.results[0].geometry.location.lat;
        longitude = response.results[0].geometry.location.lng;

       $('#latitude').val(latitude);
       $('#longitude').val(longitude);
        $('#zoom').val(zoom);

        return true;
    }else{
        return false;
    }
};

var init = function(){
    // on affiche la carte
    map = L.map('map').setView([latitude, longitude], zoom);
    marker = L.marker([latitude, longitude]).addTo(map);
    marker.bindPopup(ville).openPopup();

    //Ajout d'un layer de carte
    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
        attribution: '; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    //modification de la position du marker
    map.on('click', function(ev) {
        map.removeLayer(marker);
        marker = new L.marker(ev.latlng).addTo(map);
        latitude = ev.latlng.lat;
        longitude = ev.latlng.lng;
        zoom = map.getZoom();
        $('#zoom').val(zoom);
        $('#latitude').val(latitude);
        $('#longitude').val(longitude);
    });
};

coord();
init();

var submit = document.getElementById("submit");
submit.onclick = function(){
    ville = document.getElementById("newSerie").value;
    zoom = document.getElementById("zoom").value;
    if(ville == "" || zoom == ""){
        sweetAlert("Oops...", "Veuillez saisir un nom de série et un zoom !", "error");
    }else{
        if(coord()){
            map.remove();
            init();
        }else{
            sweetAlert("Oops...", "ville inconnue...!", "error");
        }
    }
};

var serie = document.getElementById("serie");
serie.onclick = function(){
    ville = document.getElementById("newSerie").value;
    if(ville == ""){
        swal("Oops...", "Veuillez saisir un nom de serie...!", "error")
    }else {
        swal("La serie a bien ete ajoutee!", "", "success")
    }
};
