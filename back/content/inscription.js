/**
 * Created by marina on 04/02/16.
 */
$(document).ready(function () {
    $('.modal-trigger').leanModal({
            dismissible: true, // Modal can be dismissed by clicking outside of the modal
            opacity: .5, // Opacity of modal background
            in_duration: 300, // Transition in duration
            out_duration: 200 // Transition out duration
        }
    );
});

var signIn = document.getElementById("signIn");
signIn.onclick = function(){
    last_name = document.getElementById("last_name").value;
    first_name = document.getElementById("first_name").value;
    email = document.getElementById("email").value;
    password = document.getElementById("password").value;
    confirmPassword = document.getElementById("confirmPassword").value;
    if(last_name != "" && first_name != "" && email != "" && password != "" && confirmPassword != ""){
        swal("Votre compte est disponible !", "", "success");
    }else {
        swal("Oops...", "Veuillez saisir toutes les informations...!", "error");
    }
};
