<?php
/**
 * Created by PhpStorm.
 * User: locoman
 * Date: 12/01/16
 * Time: 15:45
 */

require '../vendor/autoload.php';
require '../config/Connec.php';

use photolocate\back\controller\adminController;
Connec::Connection('config.ini');

// Connection à la base avec Eloquent
$app = new Slim\Slim(array(
    'view' => new Slim\Views\Twig(),
    'templates.path' => '../src/photolocate/back/templates'
));
$app->add(new \Slim\Middleware\SessionCookie(array(
    'expires' => '20 minutes',
    'path' => '/',
    'domain' => null,
    'secure' => false,
    'httponly' => false,
    'name' => 'slim_session',
    'secret' => 'CHANGE_ME',
    'cipher' => MCRYPT_RIJNDAEL_256,
    'cipher_mode' => MCRYPT_MODE_CBC
)));

$view = $app->view();
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);

$app->get('/', function() use ($app) {
    adminController::index($app);
});

$app->get('/:action', function($action) use ($app) {
    adminController::index($app, $action);
})->name('index');

$app->post('/:action', function($action) use ($app) {
    adminController::index($app, $action);
})->name('indexPost');

$app->run();