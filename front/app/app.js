var locate = angular.module('locate', ['leaflet-directive', 'ui.materialize']);

locate.filter('timeOver', function(){
    return function(item){
        if (item == 100)
            return "finished";
        else if(item >= 80)
            return "danger";
        else if(item >= 50)
            return "warning";
        else
            return "cool";
    }
});

$(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
    $('.modal-trigger').leanModal();
    $('select').material_select();

    $(document).keypress(function(e){
        if (e.which == 13){
            $("#modalbtn").click();
        }
    });
});
