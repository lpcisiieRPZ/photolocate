locate.directive('serie', ['Serie', '$location', 'serieCurrent', 'Game', '$rootScope', function(Serie, $location, serieCurrent, Game, $rootScope) {
    // pour appeler les templates
    return {
        templateUrl: 'template/serie.html',
        link: function(scope) {
            scope.select_serie = function(game) {
                serieCurrent.serie = new Serie(game.selected);
                serieCurrent.game = new Game(game);
                if(game.player === undefined)
                    serieCurrent.game.player = "Guest";
                else
                    serieCurrent.game.player = game.player;

                serieCurrent.game.serie_id = game.selected.id;
                serieCurrent.game.difficulty = game.difficult;
                $rootScope.getToken();
                $rootScope.getDifficulty();
            };
        }
    };
}]);