locate.service('Serie', ['$http', '$rootScope', function($http, $rootScope){
    // photolocate constructor
    var Serie = function(data){
        this.id = data.id;
        this.label = data.label;
        this.zoom = data.zoom;
        this.lat = data.lat;
        this.lon = data.lon;
        this.nb_photo = data.nb_photo;
    };

    return Serie;
}]);