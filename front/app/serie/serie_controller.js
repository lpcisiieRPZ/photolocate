locate.controller('SerieController',
    ['$scope', '$http', 'Serie', '$location', '$scope', '$rootScope', 'serieCurrent', function($scope, $http, Serie, $location, $scope, $rootScope, serieCurrent){

        var uri = $location.absUrl();
        var rootUri = uri.split('/');
        var i=0;
        $rootScope.root = "";
        var distance = 0;
        var mainMarker = {};
        $scope.series = [];
        $rootScope.hide_map = true;
        $scope.markers = new Array();
        var latPhoto;
        var lonPhoto;

        $scope.Nancy = {};
        //mainMarker = {
        //    lat: 48.6930000581379,
        //    lng: 6.17843627929688,
        //    opacity: 0
        //};

        while(rootUri[i] != "front"){
            $rootScope.root += rootUri[i] + '/';
            i++;
        }

        angular.extend($scope, {
            Center: {
                lat: 48.6930000581379,
                lng: 6.17843627929688,
                zoom: 13
            },
            position: {
                lat: 48.6930000581379,
                lng: 6.17843627929688
            },
            events: {},
            defaults:{
                //zoomControl : false,
                //keybord : false,
                //dragging : false,
                //doubleClickZoom : false,
                //scrollWheelZoom : false,
                //tap : false,
                //attributionControl : false,
                //zoomAnimation : false,
                //fadeAnimation : false,
                //markerZoomAnimation : false,
                //worldCopyJump : false
            }
        });

        // get the map info and create new Serie object
        $scope.load = function(){
            $http.get($rootScope.root + 'api/series')
                .then(function(response){
                        response.data.series.forEach(function(serie){
                            var newSerie = new Serie(serie);
                            $scope.series.push(newSerie);
                        });
                    },
                    function(error){
                        console.log(error);
                    }
                );
        };

        // generate the leaflet  with info
        $rootScope.generate_map = function(response){
            angular.extend($scope, {
                Center: {
                    lat: parseFloat(serieCurrent.serie.lat),
                    lng: parseFloat(serieCurrent.serie.lon),
                    zoom: parseFloat(serieCurrent.serie.zoom)
                }
            });

            latPhoto = parseFloat(response.lat);
            lonPhoto = parseFloat(response.lon);

            $rootScope.hide_map = false;
        };

        $scope.$on("leafletDirectiveMap.leaf1.click", function(event, args){
            var leafEvent = args.leafletEvent;
            var latClick = leafEvent.latlng.lat;
            var lonClick = leafEvent.latlng.lng;

            // stop the timer
            $rootScope.stopTimer();

            // calculate the distance
            $rootScope.distance(latClick, lonClick, latPhoto, lonPhoto);

            $scope.markers=[];
            $scope.markers.push({
                lat: leafEvent.latlng.lat,
                lng: leafEvent.latlng.lng
            });
        });

        $scope.load();
    }]);
