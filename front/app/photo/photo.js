/**
 * Created by locoman on 02/02/16.
 */

locate.service('Photo', ['$http', '$rootScope', function($http, $rootScope){
    // photolocate constructor
    var Photo = function(data){
        this.id = data.id;
        this.uri = data.uri;
        this.lat = data.lat;
        this.lon = data.lon;
    };

    return Photo;
}]);