locate.controller('PhotoController',
    ['$scope', '$http', 'Photo', '$location', '$rootScope', 'serieCurrent', 'leafletData', function($scope, $http, Photo, $location, $rootScope, serieCurrent, leafletData) {

        var uri = $location.absUrl();
        var rootUri = uri.split('/');
        var i = 0;
        var root = "";
        var iPic = 0;
        $scope.serieCurrent = serieCurrent;
        $scope.progress = 0;

        while (rootUri[i] != "front") {
            root += rootUri[i] + '/';
            i++;
        }

        // récupère 10 photos aléatoires de la série en cours
        $scope.load = function (serie) {
            $http.get(root + 'api/series/' + serie.id + '/randPhotos')
                .then(function (response) {
                        // stock les photos dans le tableau de photos de la série en cours
                        response.data.photos.forEach(function (photo) {
                            serieCurrent.photos.push(new Photo(photo));
                        });

                        serieCurrent.game.nb_photo = serieCurrent.photos.length;

                        // affiche la première photo
                        $rootScope.changePic();
                    },
                    function (error) {
                        console.log(error);
                        $rootScope.generate_map(serieCurrent.serie);
                    }
                );
        };

        var coordMap = $('#leaf1').offset();
        var widthMap = $('#leaf1').width();
        var divHover = $('<div>');
        divHover.css({"top": coordMap.top-30, "left": coordMap.left, "background-color": 'black', opacity: 0.8, "zIndex": 1004, width: widthMap, height: 480, position: 'absolute' });
        divHover.click(function(e){
            e.preventDefault();
        });

        // change la photo
        $rootScope.changePic = function () {
            if (iPic != serieCurrent.photos.length) {
                $rootScope.resetTimer();
                $('#MainWrap').append(divHover);
                var img = $('<img>');
                img.attr('src', root + "front/photos/" + serieCurrent.photos[iPic].uri );
                img.width('100%');
                img.load(function(event) {
                    $rootScope.timer();
                    divHover.remove();
                });

                // trigger the generation of the map
                $rootScope.generate_map(serieCurrent.photos[iPic]);
                // display photo and generate timer after full load
                $('#photo')
                    .empty()
                    .prepend(img);
                iPic++;

                leafletData.getMap().then(function(map) {
                });
            }else{
                $rootScope.display_final();
            }
        };

        // trigger au changement de la serie courante
        $scope.serieCurrent = serieCurrent;
        $scope.$watch('serieCurrent.serie', function (newValue) {
            if (newValue != 0 && newValue != undefined) {
                $scope.load(newValue);
            }
        });


    }]);
