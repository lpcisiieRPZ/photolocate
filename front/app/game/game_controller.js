locate.controller('GameController',
    ['$scope', '$http', 'Game', '$rootScope', '$interval', 'serieCurrent', function($scope, $http, Game, $rootScope, $interval, serieCurrent){

        $rootScope.score = [];
        $rootScope.totalScore = 0;
        var distance = 0;
        var time = 0;
        var totalPoints = 0;
        var distParam = 0;
        var photoNum = 0;
        $scope.markersResponse = new Array();
        $scope.markersResponseTotal = new Array();

        angular.extend($scope, {
            Center2: {
                lat: 48.6930000581379,
                lng: 6.17843627929688,
                zoom: 13
            },
            position: {
                lat: 58.6930000581379,
                lng: 6.17843627929688
            },
            events: {},
            defaults:{
                //zoomControl : false,
                //keybord : false,
                //dragging : false,
                //doubleClickZoom : false,
                //scrollWheelZoom : false,
                //tap : false,
                //attributionControl : false,
                //zoomAnimation : false,
                //fadeAnimation : false,
                //markerZoomAnimation : false,
                //worldCopyJump : false
            }
        });

        $rootScope.getDifficulty = function() {
            switch (true) {
                case (serieCurrent.game.difficulty == 1):
                    distParam = 300;
                    break;

                case (serieCurrent.difficulty == 2):
                    distParam = 200;
                    break;

                case (serieCurrent.difficulty == 3):
                    distParam = 150;

                    break;

                default:
                    distParam = 300;
                    serieCurrent.game.difficulty = 1;
                    break;
            }
        };

        // calculate the score according to time and distance
        $scope.score = function (dist, time) {
            var points = 0;

            switch(true) {
                case (dist < distParam):
                    points = 5;
                    break;

                case (dist < distParam*2):
                    points = 3;
                    break;

                case (dist < distParam*3):
                    points = 1;
                    break;

                default:
                    points = 0;
                    break;
            }

            switch(true) {
                case (time < 2):
                    points = points*4;
                    break;

                case (time < 5):
                    points = points*2;
                    break;

                case (time >= 10):
                    points = 0;
                    break;

                default:
                    points += 0;
                    break;
            }
            photoNum += 1;
            totalPoints += points;
            this.distance = dist.toFixed(2);
            this.time = time ;
            this.points = points;
            this.scoreFinal = totalPoints;
            this.photoNum = photoNum;
            this.photoNumTotal = serieCurrent.photos.length;
            $rootScope.score.push(points);
        };

        // Timer variables
        var stop;
        $scope.interval = {value: 0};

        // begin timer
        $rootScope.timer = function () {
            if ( angular.isDefined(stop) ) return;

            stop = $interval(function () {
                $scope.interval.value = ($scope.interval.value + 1) % 100;
                //if($scope.interval.value === 10){
                //    $rootScope.stopTimer();
                //}
            }, 1000, 10);
        };

        // stop timer
        $rootScope.stopTimer = function() {
            if (angular.isDefined(stop)) {
                $interval.cancel(stop);
                stop = undefined;
                time = $scope.interval.value;
                $scope.display_score();
            }
        };

        $rootScope.stopTimerNoReload = function() {
            if (angular.isDefined(stop)) {
                $interval.cancel(stop);
                stop = undefined;
            }
        };

        // reset timer
        $rootScope.resetTimer = function(){
            $rootScope.stopTimer();
            $scope.interval = {value: 0};
        };

        // calculate the distance between the initial marker and the clicked one
        $rootScope.distance = function(lat2, lon2, lat1, lon1){

            if (typeof(Number.prototype.toRad) === "undefined") {
                Number.prototype.toRad = function() {
                    return this * Math.PI / 180;
                }
            }

            var R = 6371000; // meter
            var Phi1 = lat1.toRad();
            var Phi2 = lat2.toRad();
            var DeltaPhi = (lat2 - lat1).toRad();
            var DeltaLambda = (lon2 - lon1).toRad();

            var a = Math.sin(DeltaPhi / 2) * Math.sin(DeltaPhi / 2)
                + Math.cos(Phi1) * Math.cos(Phi2) * Math.sin(DeltaLambda / 2)
                * Math.sin(DeltaLambda / 2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
            distance = (R * c);

            $scope.score(distance, time);

            angular.extend($scope, {
                Center2: {
                    lat: lat2,
                    lng: lon2,
                    zoom: 13
                },
                position: {
                    lat: lat2,
                    lng: lon2
                }
            });

            $scope.markersResponse = [];
            $scope.markersResponse.push({
                lat: lat1,
                lng: lon1,
                message: "Response",
                focus: true,
                draggable: false
            });
            $scope.markersResponse.push({
                lat: lat2,
                lng: lon2,
                message: "Where you clicked",
                draggable: false
            });
        };

        // call modal wich display score for a photo
        $scope.display_score = function () {
            this.time = time;
            $('#modal1').openModal({
                dismissible: false, // Modal can be dismissed by clicking outside of the modal);
                complete: function() { $rootScope.changePic(); } // Callback for Modal close
            });
        };


        // call modal at last pic, ask to reload
        $rootScope.display_final = function(){
            serieCurrent.game.score = totalPoints;
            serieCurrent.game.status = 2;

            $rootScope.finish();
            $scope.bestScoreID();

            $('#modal2').openModal({
                dismissible: false, // Modal can be dismissed by clicking outside of the modal);
                complete: function(response) {
                    $rootScope.restart();
                } // Callback for Modal close
            });
        };

        // restart
        $rootScope.restart = function(){
            location.reload();
        };

        // get the token
        $rootScope.getToken = function(){
            $http.get($rootScope.root + 'api/token')
                .then(function(response){
                        serieCurrent.game.token = response.data.token;
                        sessionStorage.setItem("token", response.data.token);
                    },
                    function(error){
                        console.log(error);
                    }
                );
        };

        // send the score and game info
        $rootScope.finish = function (){
            $http.post($rootScope.root + 'api/games', serieCurrent.game)
                .then(function(response){
                    console.log(response);
                })
        };

        // retreive the wall of fame for a serie
        $scope.bestScoreID = function (){
            $http.get($rootScope.root + 'api/series/'+ serieCurrent.game.serie_id+'/games')
                .then(function(response){
                    $scope.best = response.data;
                })
        };

        $scope.return = function(){
            $rootScope.stopTimerNoReload();
            $('#modal0').openModal({
                dismissible: false // Modal can be dismissed by clicking outside of the modal);
            });
        };

        // pause the game
        $scope.pause = function(){
            $rootScope.stopTimerNoReload();
            $('#modal3').openModal({
                dismissible: false,
                complete: function(response) {
                    $rootScope.timer();
                }
            });
        };

        $scope.display_bestScore = function (){
            $http.get($rootScope.root + 'api/games')
                .then(function(response){
                    $scope.best = response.data;
                })
        };

        $rootScope.display_finalBestScore = function(){
            $scope.display_bestScore();
        };

        // retrieve the round
        $scope.continue = function(){
            $rootScope.timer();
        };
}]);
