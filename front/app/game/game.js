locate.service('Game', ['$http', '$rootScope', function($http, $rootScope){
    // photolocate constructor
    var Game = function(data){
        this.token = data.token;
        this.nb_photo = data.nb_photo;
        this.status = data.status;
        this.score = data.score;
        this.player = data.player;
        this.serie_id = data.serie_id;
        this.difficulty = data.difficulty;
    };

    return Game;
}]);