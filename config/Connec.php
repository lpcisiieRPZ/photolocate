<?php
use Illuminate\Database\Capsule\Manager as DB;

class Connec{
    public static function Connection($config_file)
    {
        $config = parse_ini_file($config_file);

        if(! $config_file)throw new \Exception("Connec::connection: echec parse $config_file <br />");
        $capsule = new DB();
        $capsule->addConnection($config);
        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }
}