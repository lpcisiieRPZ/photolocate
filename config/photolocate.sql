-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Sam 06 Février 2016 à 10:59
-- Version du serveur: 5.5.47-0ubuntu0.14.04.1
-- Version de PHP: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `photolocate`
--

-- --------------------------------------------------------

--
-- Structure de la table `apikey`
--

CREATE TABLE IF NOT EXISTS `apikey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(11) NOT NULL,
  `key` varchar(10) NOT NULL,
  `nbReq` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `apikey`
--

INSERT INTO `apikey` (`id`, `nom`, `key`, `nbReq`) VALUES
(1, 'test', 'test1234', 0);

-- --------------------------------------------------------

--
-- Structure de la table `game`
--

CREATE TABLE IF NOT EXISTS `game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `nb_photo` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `score` decimal(10,3) DEFAULT NULL,
  `player` varchar(50) DEFAULT NULL,
  `serie_id` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `difficulty` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_serie_id` (`serie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=208 ;

--
-- Contenu de la table `game`
--

INSERT INTO `game` (`id`, `token`, `nb_photo`, `status`, `score`, `player`, `serie_id`, `date`, `difficulty`) VALUES
(166, 'loctok56b4c10d8df8c2.31410478', 10, 2, 6.000, 'Guest', 1, '2016-02-05 16:35:02', 0),
(167, 'loctok56b4c1e80afff4.54894490', 10, 2, 0.000, 'htrghrh', 1, '2016-02-05 16:38:34', 1),
(173, 'loctok56b4cf3168da35.39858658', 10, 2, 8.000, 'marina', 1, '2016-02-05 17:35:17', 1),
(174, 'loctok56b4cf8a4a23b9.10410266', 10, 2, 26.000, 'marina', 1, '2016-02-05 17:36:57', 1),
(179, 'loctok56b4d9f02fced4.96775344', 10, 2, 12.000, 'Mathis', 1, '2016-02-05 18:20:56', 1),
(190, 'loctok56b5af68c8e857.00416335', 10, 2, 28.000, 'mathis', 26, '2016-02-06 09:32:18', 300),
(191, 'loctok56b5afb14b79a6.90465719', 10, 2, 60.000, 'Guest', 1, '2016-02-06 09:33:28', 1),
(203, 'loctok56b5bee0790544.07973125', 10, 2, 54.000, 'Guest', 1, '2016-02-06 10:38:17', 1),
(204, 'loctok56b5bf7c642d59.86760868', 10, 2, 66.000, 'alex', 26, '2016-02-06 10:40:42', 1),
(205, 'loctok56b5bfa7bb16f0.53787167', 10, 2, 62.000, 'alex', 1, '2016-02-06 10:41:32', 1),
(207, 'loctok56b5c329722ab1.88132480', 10, 2, 46.000, 'Guest', 1, '2016-02-06 10:57:02', 1);

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

CREATE TABLE IF NOT EXISTS `photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uri` varchar(50) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `serie_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_serie_id` (`serie_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Contenu de la table `photo`
--

INSERT INTO `photo` (`id`, `uri`, `lat`, `lon`, `serie_id`) VALUES
(4, 'Nancy/cinema.jpg', 48.691425, 6.195976, 1),
(5, 'Nancy/fac_science.jpg', 48.666963, 6.159675, 1),
(6, 'Nancy/gare.jpg', 48.68982, 6.174457, 1),
(7, 'Nancy/jardin_dominiqueAlex.jpg', 48.694807, 6.189096, 1),
(8, 'Nancy/musee_aquarium.jpg', 48.69494, 6.188219, 1),
(9, 'Nancy/nations.jpg', 48.662113, 6.174903, 1),
(10, 'Nancy/parc_pepiniere.jpg', 48.698017, 6.185024, 1),
(11, 'Nancy/place_carriere.jpg', 48.695728, 6.181731, 1),
(12, 'Nancy/place_stanislas.jpg', 48.693575, 6.183258, 1),
(13, 'Nancy/porte_craffe.jpg', 48.698868, 6.177742, 1),
(14, 'Nancy/porte_sainte_catherine.jpg', 48.695572, 6.189937, 1),
(15, 'Nancy/saint_epvre.jpg', 48.696274, 6.179482, 1),
(16, 'Nancy/saint_sebastien.jpg', 48.687934, 6.181194, 1),
(17, 'Nancy/stade.jpg', 48.695373, 6.21062, 1),
(18, 'Nancy/thermal.jpg', 48.67895, 6.168435, 1),
(19, 'Nancy/villa_majorelle.jpg', 48.685553, 6.16393, 1),
(24, 'Strasbourg/cathedrale.jpg', 48.581979, 7.751244, 26),
(25, 'Strasbourg/cimetiere_nord.jpg', 48.610711, 7.775762, 26),
(26, 'Strasbourg/gare.jpg', 48.585385, 7.734227, 26),
(27, 'Strasbourg/halles.jpg', 48.586432, 7.741666, 26),
(28, 'Strasbourg/homme_fer.jpg', 48.584053, 7.744534, 26),
(29, 'Strasbourg/musée_archeologique.jpg', 48.580981, 7.752455, 26),
(30, 'Strasbourg/parc_orangerie.jpg', 48.591287, 7.776176, 26),
(31, 'Strasbourg/parlement.jpg', 48.597488, 7.768482, 26),
(32, 'Strasbourg/passerelle.jpg', 48.5691, 7.803635, 26),
(33, 'Strasbourg/place_kleber.jpg', 48.583427, 7.745845, 26),
(34, 'Strasbourg/ponts_couverts.jpg', 48.579894, 7.73939, 26),
(35, 'Strasbourg/rivetoile.jpg', 48.573269, 7.759344, 26),
(36, 'Strasbourg/stade_meinau.jpg', 48.559961, 7.75511, 26),
(37, 'Strasbourg/ugc.jpg', 48.572989, 7.763833, 26),
(38, 'Strasbourg/univsersite.jpg', 48.579019, 7.76713, 26),
(39, 'Strasbourg/vaisseau.jpg', 48.572767, 7.772183, 26);

-- --------------------------------------------------------

--
-- Structure de la table `serie`
--

CREATE TABLE IF NOT EXISTS `serie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(50) NOT NULL,
  `map_id` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL,
  `zoom` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_serie_id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Contenu de la table `serie`
--

INSERT INTO `serie` (`id`, `label`, `map_id`, `lat`, `lon`, `zoom`) VALUES
(1, 'Nancy', 0, 48.692054, 6.184417, 14),
(26, 'Strasbourg', 0, 48.5734053, 7.752111299999999, 14);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `date_connect` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `email`, `password`, `date_connect`) VALUES
(1, 'admin', 'admin', 'test@gmail.com', '$2y$10$EhatOZeny1slyuwNV7/nyuhr5E0F.4/XBUhyy03gNEgItHJX.DUgO', '2016-02-04');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `game`
--
ALTER TABLE `game`
  ADD CONSTRAINT `game_ibfk_1` FOREIGN KEY (`serie_id`) REFERENCES `serie` (`id`);

--
-- Contraintes pour la table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `photo_ibfk_1` FOREIGN KEY (`serie_id`) REFERENCES `serie` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
