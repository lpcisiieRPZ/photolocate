<?php

require '../vendor/autoload.php';
require '../config/Connec.php';
require '../src/photolocate/common/apiKeyFunction.php';


use photolocate\api\controller\gameController;
use photolocate\api\controller\seriesController;
use photolocate\api\controller\indexController;



// Connection à la base avec Eloquent
Connec::Connection('config.ini');
$app = new Slim\Slim;

$app->get('/', function() use ($app) {
    $app->contentType('text/html; charset=utf-8');
	echo "ça marche";
});

// generate and return token. Create a party in database
$app->get('/token', function() use ($app) {
    indexController::token($app);
});

//fonction photo random
$app->get('/series/:id/randPhotos', function($id) use ($app){
    indexController::randomPic($app, $id);
});

// access to list of series
$app->get('/series', function() use ($app) {
    seriesController::series($app);
})->name('series');

// access to a serie
$app->get('/series/:id', function($id) use ($app) {
    seriesController::seriesId($app, $id);
})->name('seriesId');

// access to list of photos for a series
$app->get('/series/:id/photos', function($id) use ($app) {
    seriesController::seriesPhoto($app, $id);
})->name('seriesPhoto');

$app->get('/series/:id/photos/:id2', function($id, $id2) use ($app) {
    seriesController::seriesPhotoId($app, $id, $id2);
})->name('seriesPhotoId');

// access to parties
$app->get('/games', function() use ($app) {
    gameController::games($app);
})->name('games');

// send score for a parties
$app->post('/games', function() use ($app) {
    gameController::saveGame($app);
})->name('gamesPost');

// access to score for a series
$app->get('/series/:id/games', function($id) use ($app) {
    gameController::gamesScore($app, $id);
})->name('gamesScore');

$app->run();