<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 01/02/16
 * Time: 11:16
 */

namespace model;

class Game extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'game';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function serie_game(){
        return $this->hasMany('model\serie', 'id_serie');
    }
}