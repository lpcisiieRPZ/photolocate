<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 01/02/16
 * Time: 11:16
 */

namespace model;

class User extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'user';
    protected $primaryKey = 'id';
    public $timestamps = false;
}