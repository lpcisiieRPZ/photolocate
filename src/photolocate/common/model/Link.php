<?php
/**
 * Created by PhpStorm.
 * User: locoman
 * Date: 01/02/16
 * Time: 16:20
 */

namespace model;


class Link
{
    protected $table = 'link';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function photos(){
        return $this->hasMany('model\Photo', 'id_photo');
    }

    public function series(){
        return $this->hasMany('model\Serie', 'id_serie');
    }
}