<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 01/02/16
 * Time: 11:16
 */

namespace model;

class Photo extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'photo';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function serie_photo(){
        return $this->hasMany('model\serie', 'id_serie');
    }
}