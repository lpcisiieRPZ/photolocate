<?php

/**
 * Created by PhpStorm.
 * User: marina
 * Date: 01/02/16
 * Time: 11:16
 */

namespace model;

class Serie extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'serie';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function game(){
        return $this->belongsTo('model\game', 'id');
    }
//facion query = fasse une query
    public function photo(){
        return $this->belongsTo('model\photo', 'id');
    }
}