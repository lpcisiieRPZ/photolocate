<?php

use model\User;

function check_api_key()
{
    $app = \Slim\Slim::getInstance();
    $app->response->headers->set('Content-Type', 'application/json');
    $api_key = $app->request->get('apikey');

    // crée une variable pour récupérer la valeur de la table key
    if (isset ($api_key)) {
        $ak = User::where('key', '=', $api_key)->first();
        if (is_object($ak)) {
            $ak->nbReq += 1;
            $ak->save();
        } else {
            $app->response->setStatus(403);
            echo json_encode(array(
                "Error" => 403,
                "Message" => "Key not valid"
            ));
            $app->stop();
        }
    } else {
        $app->response->setStatus(400);
        echo json_encode(array(
            "Error" => 400,
            "Message" => "Key not found"
        ));
        $app->stop();
    }
}
