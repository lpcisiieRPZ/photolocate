<?php

namespace photolocate\api\controller;

use Illuminate\Support\Facades\DB;
use model\Annonce;
use model\Game;
use model\Photo;

class indexController
{
	public static function token($app)
	{
		$app->response->headers->set('Content-Type', 'application/json');
		$app->response->setStatus(200);
        $app->token = uniqid('loctok', true);

        $game = new Game();
        $game->token = $app->token;
        $game->status = 0;
        $game->save();

		echo json_encode(array(
			"Object" => "token",
			"token" => $app->token
		));
	}

	public static function randomPic($app, $id)
	{
        $app->response->headers->set('Content-Type', 'application/json');
        if(Photo::select('id')->where('serie_id', '=', filter_var($id, FILTER_SANITIZE_NUMBER_INT))->get()->isEmpty()){
            $app->response->setStatus(400);
            echo json_encode(array(
                "Error" => 400,
                "Object" => "photos",
                "Message" => "Cette serie n'a pas encore de photo"
            ));
        }else{
            $app->response->setStatus(200);
            $nbPhotos = 10;
            $photos = json_decode(Photo::select()->where('serie_id', '=', filter_var($id, FILTER_SANITIZE_NUMBER_INT))->get());
            $photosSelected = [];

            for($i=0; $i < $nbPhotos; $i++){

                // on choisi un nombre random tant qu'il y a un doublon
                do{
                    $rand = array_rand($photos);
                    $selected = $photos[$rand];
                }while(in_array($selected, $photosSelected));
                array_push($photosSelected, $selected);
            }

            $obj = array('photos' => $photosSelected);
            echo json_encode($obj);
        }

	}
}