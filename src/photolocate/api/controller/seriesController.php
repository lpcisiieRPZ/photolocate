<?php

namespace photolocate\api\controller;

use model\Serie;
use model\Photo;


class seriesController
{
    //retourne la liste de toutes les routes
    public static function series($app)
    {
        if (Serie::all()) {
            $app->response->headers->set('Content-Type', 'application/json');
            $serie = Serie::all();
            if (count($serie) != 0) {
                for ($i = 0; $i < count($serie); $i++) {
                    $serie[$i]->links = array(
                        "self" => array(
                            "href" => $app->urlFor('seriesId', ['id' => $serie[$i]->id])
                        )
                    );
                }
                $obj = array(
                    "series" => $serie
                );
                echo json_encode($obj);
            };
            $app->response->setStatus(201);
        } else {
            $app->response->setStatus(400);
            echo json_encode(array(
                "Error" => 400,
                "Object" => "id",
                "Message" => "Aucune serie"
            ));
        }
    }

    //retourne une liste en fonction de son ID
    public static function seriesId($app, $id)
    {
        if(!filter_var($id, FILTER_VALIDATE_INT) === false) {
            $app->response->headers->set('Content-Type', 'application/json');
            if (Serie::find($id)) {
                $serie = Serie::find($id);
                $obj = array(
                    "serie" => $serie,
                    "links" => array(
                        "Photo" => array(
                            "href" => $app->urlFor('seriesPhoto', ['id' => $serie->id])
                        ),
                        "Serie" => array(
                            "href" => $app->urlFor('series')
                        )
                    )
                );
                echo json_encode($obj);
                $app->response->setStatus(201);
            } else {
                $app->response->setStatus(400);
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "id",
                    "Message" => "Cet ID de serie n'existe pas"
                ));
            }
        }
    }

    //retourne les photos d'une serie
    public static function seriesPhoto($app, $id)
    {
        if(!filter_var($id, FILTER_VALIDATE_INT) === false) {
            $app->response->headers->set('Content-Type', 'application/json');
            if (Serie::find($id)) {
                $photo =  Photo::select('id', 'uri', 'lon', 'lat')->where('serie_id', '=',$id)->get();
                if (count($photo) != 0) {
                    for ($i = 0; $i < count($photo); $i++) {
                        $photo[$i]->links = array(
                            "self" => array(
                                "href" => $app->urlFor('seriesPhotoId', ['id' => $id, 'id2' =>$photo[$i]->id])
                            )
                        );
                    }
                    $obj = array(
                        "photo" => $photo,
                        "link" => array(
                            "href" => $app->urlFor('seriesId', ['id' => $id])
                        )
                    );
                    echo json_encode($obj);
                };
                $app->response->setStatus(201);
            } else {
                $app->response->setStatus(400);
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "id",
                    "Message" => "Cet ID de serie n'existe pas"
                ));
            }
        }
    }

    //retourne une photos d'une serie
    public static function seriesPhotoId($app, $id, $id2)
    {
        if(!filter_var($id, FILTER_VALIDATE_INT) === false) {
            $app->response->headers->set('Content-Type', 'application/json');
            if (Serie::find($id)) {
                if(!filter_var($id2, FILTER_VALIDATE_INT) === false){
                    if(Photo::find($id2)){
                        $photo = Photo::select('id', 'uri', 'lon', 'lat')->where('id', '=',$id2)->get();
                        if (count($photo) != 0) {
                            $obj = array(
                                "photo" => $photo,
                                "link" => array(
                                    "href" => $app->urlFor('seriesPhoto', ['id' => $id])
                                )
                            );
                            echo json_encode($obj);
                        };
                        $app->response->setStatus(201);
                    }
                }
                else{
                    $app->response->setStatus(400);
                    echo json_encode(array(
                        "Error" => 400,
                        "Object" => "id",
                        "Message" => "Cet ID de photo n'existe pas"
                    ));
                }
            } else {
                $app->response->setStatus(400);
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "id",
                    "Message" => "Cet ID de serie n'existe pas"
                ));
            }
        }
    }
}