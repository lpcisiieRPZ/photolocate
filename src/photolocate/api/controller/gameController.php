<?php

namespace photolocate\api\controller;

use Illuminate\Support\Facades\DB;
use model\Game;
use model\Serie;


class gameController
{
    public static function games($app)
    {
        $app->response->headers->set('Content-Type', 'application/json');
        if (!Game::select('player', 'score')->take(10)->orderBy('score', 'DESC')->get()->isEmpty()) {
            $app->response->setStatus(200);
            $bestScore = Game::select('player', 'score', 'date', 'difficulty', 'serie.label')->where('status', '=', '2')->take(10)->join('serie', 'game.serie_id', '=', 'serie.id')->orderBy('score', 'DESC')->get();
            echo $bestScore;
        } else {
            $app->response->setStatus(500);
            echo json_encode(array(
                "Error" => 500,
                "Object" => "highscore",
                "Message" => "unable to retreive highscore",
            ));
        }

    }

    public static function saveGame($app)
    {
        $app->response->headers->set('Content-Type', 'application/json');
        $data = json_decode($app->request->getBody());

        if (isset($data->token) && !Game::select('id')->where('token', '=', filter_var($data->token, FILTER_SANITIZE_STRING))->get()->isEmpty()) {
            if (isset($data->player) && isset($data->score) && isset($data->serie_id) && isset($data->difficulty)) {
                $game = Game::select('id')->where('token', '=', filter_var($data->token, FILTER_SANITIZE_STRING))->first();
                $game->player = filter_var($data->player, FILTER_SANITIZE_STRING);
                $game->nb_photo = filter_var($data->nb_photo, FILTER_SANITIZE_NUMBER_INT);
                $game->status = 2;
                $game->score = filter_var($data->score, FILTER_SANITIZE_NUMBER_FLOAT);
                $game->serie_id = filter_var($data->serie_id, FILTER_SANITIZE_NUMBER_INT);
                $game->date = date("Y-m-d H:i:s");
                $game->difficulty = filter_var($data->difficulty, FILTER_SANITIZE_NUMBER_INT);

                $game->save();
            } else {
                echo json_encode(array(
                    "Error" => 400,
                    "Object" => "parameters",
                    "Message" => "missing parameters"
                ));
            }
        } else {
            echo json_encode(array(
                "Error" => 400,
                "Object" => "token",
                "Message" => "invalid token"
            ));
        }
    }

    public static function gamesScore($app, $id)
    {
        $app->response->headers->set('Content-Type', 'application/json');
        if (!Game::select('player', 'score')->where("serie_id", "=", $id)->take(10)->orderBy('score', 'DESC')->get()->isEmpty()) {
            $app->response->setStatus(200);
            $bestScore = Game::select('player', 'score')->where('status', '=', '2')->where("serie_id", "=", $id)->take(10)->orderBy('score', 'DESC')->get();
            echo $bestScore;
        } else {
            $app->response->setStatus(500);
            echo json_encode(array(
                "Error" => 500,
                "Object" => "highscore",
                "Message" => "unable to retreive highscore",
            ));
        }

    }
}