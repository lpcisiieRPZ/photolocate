<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 01/02/16
 * Time: 15:04
 */


namespace photolocate\back\controller;

use model\Serie;
use model\Photo;
use model\User;

use Illuminate\Support\Facades\DB;


class adminController
{
    public static function index($app, $action = null){

        if(isset($action)){
            switch($action){
                case 'deconnection' :
                    self::deconnection($app);
                    break;
                case 'index' :
                    self::index2($app);
                    break;
                case 'connection' :
                    self::connection($app);
                    break;
                case 'inscription' :
                    self::inscription($app);
                    break;
                case 'admin' :
                    self::admin($app);
                    break;
                case 'series' :
                    self::series($app);
                    break;
                case 'addSeries' :
                    self::addSeries($app);
                    break;
                case 'photos' :
                    self::photos($app);
                    break;
                case 'addPhoto' :
                    self::addPhotos($app);
                    break;
                case 'upload' :
                    self::upload($app);
                    break;
                default :
                    self::index2($app);
                    break;
            }
        }else
            self::index2($app);

    }

    public static function index2($app)
    {
        if (isset($_SESSION['privilege'])) {
            if (session_status() === PHP_SESSION_ACTIVE && $_SESSION['privilege'] === "admin") {
                $app->redirect($app->urlFor('index', array("action"=>"admin")));;
            } else {
                $app->render('index.html.twig');
            }
        } else
            $app->render('index.html.twig');
    }

    public static function deconnection($app)
    {
        session_destroy();
        $_SESSION['privilege'] = NULL;
        $app->render('index.html.twig');
    }

    public static function connection($app)
    {
        if ($app->request->headers->get('Content-Type', 'application/x-www-form-urlencoded')) {
            $email = filter_var($app->request->post('email'), FILTER_SANITIZE_STRING);
            $password = filter_var($app->request->post('password'), FILTER_SANITIZE_STRING);

            if (isset($email) && isset($password)) {
                if (User::where("email", '=', $email)->first()) {
                    $user_db = User::where("email", '=', $email)->first();

                    if(password_verify($password, $user_db->password)){
                        session_destroy();
                        session_start();
                        $_SESSION['privilege'] = "admin";

                        $rootURI = $app->request->getRootUri();
                        $app->response->setStatus(200);
                        header("Location : $rootURI/admin");
                        adminController::admin($app);

                    } else
                        echo "wrong credential";
                } else
                    echo "erreur email";
            }
        }
    }

    public static function inscription($app)
    {
        $data = $app->request->post();
        if (($data['first_name'] != "" && $data['last_name'] != "" && $data['email'] != "" && $data['password'] != "" && $data['confirmPassword'] != "")) {
            if ($data['password'] == $data['confirmPassword']) {

                $user_db = new User();
                $user_db->first_name = filter_var($data['first_name'], FILTER_SANITIZE_STRING);
                $user_db->last_name = filter_var($data['last_name'], FILTER_SANITIZE_STRING);
                $user_db->email = filter_var($data['email'], FILTER_SANITIZE_STRING);
                $user_db->password = password_hash($data['password'], PASSWORD_DEFAULT);
                $user_db->date_connect = date('Y/m/d');

                $user_db->save();
                sleep(2.5);
                $app->redirect($app->urlFor('index', array("action"=>"index2")));
            } else {
                sleep(2.5);
                $app->redirect($app->urlFor('index', array("action"=>"index2")));
            }
        } else {
            $app->response->setStatus(500);
            json_encode(array(
                "Error" => 500,
                "Message" => "L'enregistrement a echoue"
            ));
            sleep(2.5);
            $app->redirect($app->urlFor('index', array("action"=>"index2")));

        }
    }

    public static function admin($app)
    {
        if (isset($_SESSION['privilege'])) {
            if (session_status() === PHP_SESSION_ACTIVE && $_SESSION['privilege'] === "admin") {

                $photo = Photo::all();
                $app->render('admin.html.twig', array(
                    'photos' => $photo
                ));
            } else
                $app->redirect($app->urlFor('index', array("action"=>"index2")));
        } else
            $app->redirect($app->urlFor('index', array("action"=>"index2")));
    }

    public static function series($app)
    {
        if(isset($_SESSION['privilege'])){
            if(session_status() === PHP_SESSION_ACTIVE && $_SESSION['privilege'] === "admin"){
                $serie = Serie::all();
                $app->render('series.html.twig', array(
                    'series' => $serie
                ));
            }else
                $app->redirect($app->urlFor('index', array("action"=>"index2")));
        }else
            $app->redirect($app->urlFor('index', array("action"=>"index2")));
    }

    public static function addSeries($app)
    {
        if(isset($_SESSION['privilege'])){
            if(session_status() === PHP_SESSION_ACTIVE && $_SESSION['privilege'] === "admin"){
                $data = $app->request->post();
                if($data['newSerie'] != "" && $data['latitude'] != "" && $data['longitude'] != "") {
                    $serie = new Serie();
                    $serie->label = filter_var($data['newSerie'], FILTER_SANITIZE_STRING);
                    $serie->lat = filter_var($data['latitude'], FILTER_SANITIZE_STRING);
                    $serie->lon = filter_var($data['longitude'], FILTER_SANITIZE_STRING);

                    if($data['zoom'] != 0){
                        $serie->zoom = filter_var($data['zoom'], FILTER_SANITIZE_NUMBER_INT);
                    }else{
                        $serie->zoom = 12;
                    }
                    $serie->save();
                    if(!is_dir('../front/photos/'.$serie->label)){

                        mkdir("../front/photos/".$serie->label, 0755);
                        chmod("../front/photos/".$serie->label, 0777);
                    }

                    sleep(2.5);
                    $app->redirect($app->urlFor('index', array('action' => 'index2')));
                } else {
                    $app->response->setStatus(500);
                    json_encode(array(
                        "Error" => 500,
                        "Message" => "L'enregistrement a échoué"
                    ));
                    sleep(2.5);
                    $app->redirect($app->urlFor('index', array("action"=>"series")));
                }
            }else
                $app->redirect($app->urlFor('index', array("action"=>"index2")));
        }else
            $app->redirect($app->urlFor('index', array("action"=>"index2")));

    }

    public static function photos($app)
    {
        if(isset($_SESSION['privilege'])){
            if(session_status() === PHP_SESSION_ACTIVE && $_SESSION['privilege'] === "admin"){
                $serie = Serie::all();
                $app->render('photos.html.twig', array(
                    'series' => $serie
                ));
            }else
                $app->redirect($app->urlFor('index', array("action"=>"index2")));
        }else
            $app->redirect($app->urlFor('index', array("action"=>"index2")));
    }

    public static function addPhotos($app)
    {
        if(isset($_SESSION['privilege'])){
            if(session_status() === PHP_SESSION_ACTIVE && $_SESSION['privilege'] === "admin"){
                $data = $app->request->post();
                if($data['serie'] != 0 && $data['latitude'] != "" && $data['longitude'] != "" && $_FILES['file']['name'] != "") {
                    $photo = new Photo();

                    $serie_label = Serie::select('label')->where('id', '=', $data['serie'])->first();
                    self::upload($serie_label->label);
                    $fileUpload = filter_var($serie_label->label.'/'.$_FILES['file']['name'], FILTER_SANITIZE_STRING);
                    //chmod($fileUpload, 0775);
                    $photo->uri = $fileUpload;
                    $photo->lat = filter_var($data['latitude'], FILTER_SANITIZE_STRING);
                    $photo->lon = filter_var($data['longitude'], FILTER_SANITIZE_STRING);
                    $photo->serie_id = filter_var($data['serie'], FILTER_SANITIZE_STRING);

                    $photo->save();

                    sleep(2.5);
                    $app->redirect($app->urlFor('index', array("action"=>"admin")));
                } else {
                    $app->response->setStatus(500);
                    json_encode(array(
                        "Error" => 500,
                        "Message" => "L'enregistrement a échoué"
                    ));
                    sleep(2.5);
                    $app->redirect($app->urlFor('index', array("action"=>"photos")));
                }
            }else
                $app->redirect($app->urlFor('index', array("action"=>"index2")));
        }else
            $app->redirect($app->urlFor('index', array("action"=>"index2")));
    }

    public static function upload($serie){

        if(!is_dir('../front/photos/'.$serie)){
            mkdir("../front/photos/".$serie, 0755);
            chmod("../front/photos/".$serie, 0777);
        }

        $storage = new \Upload\Storage\FileSystem('../front/photos/'.$serie);
        $file = new \Upload\File('file', $storage);

        // Optionally you can rename the file on upload
        //$new_filename = uniqid();
        //$file->setName($new_filename);

        // Validate file upload
        // MimeType List => http://www.iana.org/assignments/media-types/media-types.xhtml
        $file->addValidations(array(
            // Ensure file is of type "image/png"
            new \Upload\Validation\Mimetype(array('image/png', 'image/jpg', 'image/gif', 'image/jpeg')),

            //You can also add multi mimetype validation
            //new \Upload\Validation\Mimetype(array('image/png', 'image/gif'))

            // Ensure file is no larger than 5M (use "B", "K", M", or "G")
            new \Upload\Validation\Size('5M')
        ));

        // Access data about the file that has been uploaded
        $data = array(
            'name' => $file->getNameWithExtension(),
            'extension' => $file->getExtension(),
            'mime' => $file->getMimetype(),
            'size' => $file->getSize(),
            'md5' => $file->getMd5(),
            'dimensions' => $file->getDimensions()
        );

        // Try to upload file
        try {
            // Success!
            $file->upload();
        } catch (\Exception $e) {
            // Fail!
            $errors = $file->getErrors();
        }
    }
}